package com.fromzero.pokemongo

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.Toast

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        checkPermision()
        //loadPockemons()
    }

    var ACCESSLOCATION=123

    fun checkPermision()
    {

        if (Build.VERSION.SDK_INT>=23)
        {
            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),ACCESSLOCATION)
                return
            }
        }
        GetUserLocation()
    }

    fun GetUserLocation()
    {
        Toast.makeText(this, "User location access on", Toast.LENGTH_LONG).show()

        var myLocation = MyLocationListener()

        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3, 3f, myLocation)
        var myThread = myThread()
        myThread.start()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {

        when(requestCode)
        {
            ACCESSLOCATION->
            {
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    GetUserLocation()
                }
                else
                {
                    Toast.makeText(this,"We cannot access to your location", Toast.LENGTH_SHORT).show()
                }
            }
        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onMapReady(googleMap: GoogleMap)
    {
        mMap = googleMap

        // Add a marker in Sydney and move the camera

    }


    var location:Location?=null

    inner class MyLocationListener:LocationListener
    {

        constructor(){
            location = Location("Start")
            location!!.latitude = 0.0
            location!!.longitude = 0.0
        }


        override fun onLocationChanged(p0: Location?) {
            location = p0
        }

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onProviderEnabled(p0: String?) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun onProviderDisabled(p0: String?) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }


    var oldLocation:Location?=null
    inner class myThread:Thread
    {
        constructor():super()
        {
            oldLocation = Location("Start")
            oldLocation!!.latitude = 0.0
            oldLocation!!.longitude = 0.0
        }

        override fun run()
        {
            while (true)
            {
                try
                {
                    if (oldLocation!!.distanceTo(location)==0f)
                    {
                        continue
                    }
                    if (PockemonLists.size == 0)
                    {
                        loadPockemons()
                    }

                    oldLocation = location
                    runOnUiThread{


                        //show me
                        mMap!!.clear()
                        val sydney = LatLng(location!!.latitude, location!!.longitude)
                        mMap.addMarker(MarkerOptions().
                                position(sydney)
                                .title("Игрок")
                                .snippet("Мощь:$PlayerPower")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ash)))

                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14f))

                        //show pockemons
                        for (v in PockemonLists)
                        {
                            if (v.isCatch == false)
                            {
                                val newPock = LatLng(v.location!!.latitude,v.location!!.longitude)
                                mMap.addMarker(MarkerOptions().
                                        position(newPock)
                                        .title(v.name)
                                        .snippet("Мощь: ${v.power}")
                                        .icon(BitmapDescriptorFactory.fromResource(v.image!!)))

                                //catching
                                if (location!!.distanceTo(v.location)<2f)
                                {
                                    v.isCatch = true
                                    PlayerPower += v.power!!
                                    Toast.makeText(applicationContext, "${v.name} - пойман! Ваша мощь теперь - $PlayerPower",
                                            Toast.LENGTH_LONG).show()
                                }
                            }
                        }




                    }

                    Thread.sleep(10000)
                }
                catch ( ex:Exception){}
            }
        }
    }
    var PlayerPower = 0.0
    var PockemonLists = ArrayList<Pockemon>()

    fun loadPockemons()
    {
        PockemonLists.add(Pockemon(R.drawable.pok1, "Пикачу", "-", 69.0, location!!.latitude + rand(), location!!.longitude + rand()))
        PockemonLists.add(Pockemon(R.drawable.pok2, "Кубон", "-", 50.0, location!!.latitude + rand(), location!!.longitude + rand()))
        PockemonLists.add(Pockemon(R.drawable.pok3, "Слоупок", "-", 49.0, location!!.latitude + rand(), location!!.longitude + rand()))
        PockemonLists.add(Pockemon(R.drawable.pok4, "Вартортл", " -", 30.0, location!!.latitude + rand(), location!!.longitude + rand()))
    }

    fun  rand():Double
    {
        var rangeMin = -0.09
        var rangeMax = 0.09

        val rand = Random()
        return rangeMin + (rangeMax - rangeMin) * rand.nextDouble()
    }

}
